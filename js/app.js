import {Platform} from 'react-native';
import {Navigation} from 'react-native-navigation';
import registerScreens from './screens';

// screen related book keeping
registerScreens();

// this will start our app
Navigation.startSingleScreenApp({
	screen: {
		screen: 'mx.somosdar.Splash',
		title: 'Inicio', 
		navigatorStyle: {}, 
		navigatorButtons: {} 
	},
	appStyle: {
		navBarButtonColor: '#ffffff',
		tabBarButtonColor: '#ffffff',
		navBarTextColor: '#ffffff',
		navigationBarColor: '#E52427',
		navBarBackgroundColor: '#E52427',
		statusBarColor: '#17AFBD',
	},
	drawer: {
		left: {
			screen: 'mx.somosdar.Drawer',
			passProps: {},
		},
		disableOpenGesture: false 
	},
	passProps: {},
	animationType: 'slide-down' 
});
