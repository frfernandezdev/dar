import React, { Component } from 'react';
import {
    StyleSheet,
    Alert,
    View, 
    Text,
    Image,
    TextInput,
    TouchableNativeFeedback,
    Button
} from 'react-native'; 
import {Navigation} from 'react-native-navigation';

const btnStyle = {
    textColor: '#e52427'
};

const API_URL = 'https://74.208.239.234:8082/users';

class Acount extends Component {
    constructor(props) {
        super(props);
        this.dataSource = {};
    }

    handlerSubmit() {
        if(this.dataSource!=null) {
            fetch(API_URL, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.dataSource)
            })
            .then((response) => {
                console.log(response);
                if (response.e) {
                    console.log(response.item);
                }else {
                    alert(response.m);
                    console.log(response.m);
                }
            }) 
            .catch((err) => {
                console.error(err);
            }) 
        }

    }

    handlerOptions() {
        this.props.navigator.push({
            screen: 'dar.login', // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Registrarte</Text>
                <View style={styles.content}>
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="Nombre"
                    />
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="Apellido Paterno"
                    />
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="Apellido Materno"
                    />
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="Telefono"
                    />
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="Estado"
                    />
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="Ciudad"
                    />
                    <TextInput
                        style={styles.inputText}
                        onChangeText={(text) => this.dataSource.name = text}
                        placeholder="C.P."
                    />
                    <TouchableNativeFeedback onPress={() => this.handlerSubmit()}>
                            <View style={styles.btn}>
                                <Text style={styles.btn_text}>Entrar</Text>
                            </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback  onPress={() => this.handlerOptions()}>
                        <View style={styles.btn}>
                            <Text style={styles.btn_text}>Registrate</Text>
                        </View>
                    </TouchableNativeFeedback>           
                </View>
            </View>
        );
    }
}

// <SectionList
//     renderItem={({item}) => <ListItem title="Estado" />}
//     renderSectionHeader={({section}) => <H1 title={section.title} />}
//     sections={[ // homogenous rendering between sections
//         {data: [...], title: ...},
//         {data: [...], title: ...},
//         {data: [...], title: ...},
//     ]}
// />

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#17afbd',
    },
    content: {
        marginTop: 40,
        width: 280,
        height: 'auto',
    },
    title: {
        fontSize: 20,
        color: 'white',
    },
    inputText: {
        backgroundColor: '#99e5f3',
        color: 'white',
        marginBottom: 4,
        borderBottomColor: 'transparent',
        paddingLeft: 5,
        paddingRight: 5,
        width: 266,
        marginLeft: 7,
    },
    btn: {
        width: 300,
        borderRadius: 15,

    }
})
   
export default Acount;