import React from 'react';
import { View, Image, StyleSheet, Dimensions } from 'react-native';
import { Container, Content } from 'native-base';


import store from 'react-native-simple-store';
var mStore = store;

class MyClass extends React.Component {
	constructor(props) {
		super(props);

		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false
		});
		
		this.props.navigator.setDrawerEnabled({
			side: 'left',
			enabled: false
		});
		
		setTimeout(function(){
			mStore.get('token')
			.then((token) => {
				console.log("token", token);
				let page;
				if(token && token.token && token.tokenSecret)
					page = 'dar.main';
				else
					page = 'dar.login';	
				
				this.props.navigator.resetTo({
					screen: page,
					animated: true,
					animationType: 'fade'
				});
				
			}).catch((err) => {
				this.props.navigator.resetTo({
					screen: 'dar.login',
					animated: true,
					animationType: 'fade'
				});
			});
			
		}.bind(this),5000)
		
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={{flexDirection: 'row', flex: 1}}>
					<View style={styles.leftBar}>
					</View>
					<View style={styles.rightBar}>
				</View>
				<View style={styles.overlay}>
					<Image
						resizeMode={'cover'}
						style={{width: Dimensions.get('window').width * 0.9}}
						source={require("./img/logo.png")}
					/>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#ffffff',
	},
	leftBar: {
		flex: 1, 
		height: Dimensions.get('window').height, 
		backgroundColor:"#E52427"
	},
	rightBar: {
		flex: 1, 
		height: Dimensions.get('window').height, 
		backgroundColor:"#17AFBD"
	},
	overlay: {
		position: 'absolute',
		top:0,
		bottom:0,
		right: 0,
		left: 0,
		alignItems: 'center',
	}
});

export default MyClass;
