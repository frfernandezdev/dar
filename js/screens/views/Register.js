import {Navigation} from 'react-native-navigation';
import React, { Component } from 'react';
import {
    StyleSheet,
    Alert,
    View,
    Text, 
    Image,
    TextInput,
    TouchableNativeFeedback,
    ScrollView
} from 'react-native'; 
import { Root ,Toast } from 'native-base';

import ImagePicker from 'react-native-image-crop-picker';

const btnStyle = {
    textColor: '#e52427'
};

const API_URL = 'https://api.somosdar.mx/users';

const data = new FormData();


class AcountRegister extends Component {
    constructor(props) {
        super(props);

        this.props.navigator.toggleNavBar({
            to: 'hidden', 
            animated: true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            enabled: false // should the drawer be enabled or disabled (locked closed)
        });

        this.state = {
            showToast: false,
			form: {
				name: '',
				mail: '',
				phone: '',
				rfc: '',
				password: '',
				file: {},
				address: ''
			}
        }

        this.dataSource = {};
        data.append('priv', '2');
        data.append('datasource', 'regular');
        this._ViewImage = false;
    }

    handlerSubmit() {
        if(this.state.form.file.name) {
            if (this.state.form.password === this.state.form.Confirm_password) {
                
				let formData = new FormData();
				formData.append('name', this.state.form.name);
				formData.append('mail', this.state.form.mail);
				formData.append('phone', this.state.form.phone);
				formData.append('rfc', this.state.form.rfc);
				formData.append('password', this.state.form.password);
				formData.append('file', this.state.form.file);
				formData.append('address', this.state.form.address);
				formData.append('datasource', 'regular');
				formData.append('priv', '2');
				
				
				this.props.navigator.showModal({
					overrideBackPress: true,
					screen: "mx.somosdar.Loader",
					animationType: 'slide-up',
					title: "Enviando",
					passProps: {
						title: "Enviando"
					}, 
					navigatorStyle: {
						navBarHidden: true
					},
					style: {
						backgroundBlur: "dark",
						backgroundColor: "#00000080",
						tapBackgroundToDismiss: false
					}
				});
				
				
				fetch(API_URL, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'multipart/form-data',
					},
					body: formData
				})
				.then((response) => response.json())
				.then((response) => {
					Navigation.dismissModal();
					if (response.e===0) {
						console.log(response);
						this.props.navigator.push({
							screen: 'dar.login', // unique ID registered with Navigation.registerScreen
							title: undefined, // navigation bar title of the pushed screen (optional)
						});
					}else {
						Navigation.dismissModal();
						Toast.show({
							text: response.mes,
							position: 'bottom',
							buttonText: 'Okay'
						})
					}
				}) 
				.catch((err) => {
					Navigation.dismissModal();
					Toast.show({
						text: "Error de Conexión",
						position: 'bottom',
						buttonText: 'Okay'
					})
				}) 
            }else {
                Toast.show({
                    text: 'Las Contraseñas deben Coincidir',
                    position: 'bottom',
                    buttonText: 'Okay'
                })    
            }
        }else {
            Toast.show({
                text: 'Debes Enviar una Foto de Perfil',
                position: 'bottom',
                buttonText: 'Okay'
            })
        }

    }

    handlerOptions() {
        this.props.navigator.push({
            screen: 'dar.login', // unique ID registered with Navigation.registerScreen
            title: undefined, // navigation bar title of the pushed screen (optional)
        });
    }

    SelectImage() {
        ImagePicker.openPicker({
            width: 480,
			height: 480,
            cropping: true
        }).then(image => {
			this.setState({
				form: {
					...this.state.form,
					file: { 
						name: 'photoProfile.jpg',
						type: image.mime, 
						uri: image.path 
					}
				}
			})
        });
    }

    render() {
        return (
			<Root>
				<ScrollView style={styles.scrollView}>
                    <View style={styles.container}>
                        
						<Image
							resizeMode={'center'}
							style={{width: 280, height:280}}
							source={require("./img/logorojo.png")}
						/>
                        <View style={styles.content}>
                            <TextInput
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										name: text,
									}
								})}
								value={this.state.form.name}
                                placeholder="Nombre"
                            />
                            <TextInput
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										mail: text,
									}
								})}
								value={this.state.form.mail}
								keyboardType={'email-address'}
                                placeholder="Correo Electronico"
                            />
                            <TextInput
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										phone: text,
									}
								})}
								keyboardType={'phone-pad'}
								value={this.state.form.phone}
                                placeholder="Numero de Telefono"
                            />
                            <TextInput
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										rfc: text,
									}
								})}
								keyboardType={'phone-pad'}
								value={this.state.form.rfc}
                                placeholder="RFC"  
                            />
                            <TextInput
                                editable = {true}
                                maxLength = {300}
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										address: text,
									}
								})}
								value={this.state.form.address}
                                placeholder="Direccion"
                            />
                            <TextInput
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										password: text,
									}
								})}
								value={this.state.form.password}
                                placeholder="Contraseña"
                                secureTextEntry={true}
                            />
                            <TextInput
                                style={styles.inputText}
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										Confirm_password: text,
									}
								})}
								value={this.state.form.Confirm_password}
                                placeholder="Confirmar Contraseña"
                                secureTextEntry={true}
                            />
                            <TouchableNativeFeedback  onPress={() => this.SelectImage()}>
                                <View style={styles.btn}>
                                    <Text style={styles.btn_text}>Imagen de Perfil</Text>
                                </View>
                            </TouchableNativeFeedback>   
							{
								this.state.form.file.uri
								&&
								<Image
									resizeMode={'cover'}
									style={{width: 280, height: 280}}
									source={{uri: this.state.form.file.uri}}
								/>
							}
							
                            <TouchableNativeFeedback onPress={() => this.handlerSubmit()}>
                                <View style={styles.btn}>
                                    <Text style={styles.btn_text}>Registrar</Text>
                                </View>
                            </TouchableNativeFeedback>
                            <TouchableNativeFeedback  onPress={() => this.handlerOptions()}>
                                <View style={styles.btn}>
                                    <Text style={styles.btn_text}>Iniciar Sesion</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </View>
				</ScrollView>
			</Root>
        );
    }
}

const styles = StyleSheet.create({
	scrollView: {
        flex: 1,
        backgroundColor: '#17afbd',
	},
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#17afbd',
    },
    content: {
        marginTop: 40,
        width: 280,
        height: 'auto',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        color: 'white',
    },
    inputText: {
        backgroundColor: '#99e5f3',
        color: 'white',
        marginBottom: 4,
        borderBottomColor: 'transparent',
        paddingLeft: 5,
        paddingRight: 5,
        width: 266,
        marginLeft: 7,
    },
    btn: {
        position: 'relative',
        height: 35,
        margin: 8,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
    },
    btn_text: {
        color: '#e52427',
    }
})
   
export default AcountRegister;