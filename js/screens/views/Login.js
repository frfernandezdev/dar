import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    StyleSheet,
    Alert,
    View, 
    Image,
    TextInput,
	Dimensions,
    TouchableNativeFeedback,
    Button,
    Text,
	ScrollView
} from 'react-native'; 
import { Root ,Toast } from 'native-base';

import store from 'react-native-simple-store';

const API_URL = 'https://api.somosdar.mx/auth/login';

const btnStyle = {
    textColor: '#e52427'     
};

class Login extends Component {
    constructor(props) { 
        super(props);
        this.props.navigator.toggleNavBar({
            to: 'hidden', 
            animated: true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            enabled: false // should the drawer be enabled or disabled (locked closed)
        });

        this.state = {
            showToast: false
        }

        this.dataSource = {};
    }

    handlerSubmit() {
		
        if (this.dataSource.mail && this.dataSource.password) {
			
			
			this.props.navigator.showModal({
				overrideBackPress: true,
				screen: "mx.somosdar.Loader",
				animationType: 'slide-up',
				title: "Ingresando",
				passProps: {
					title: "Ingresando"
				}, 
				navigatorStyle: {
					navBarHidden: true
				},
				style: {
					backgroundBlur: "dark",
					backgroundColor: "#00000080",
					tapBackgroundToDismiss: false
				}
			});
			
			
			
            fetch(API_URL, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.dataSource)
            })
            .then((response) => response.json()) 
            .then((response) => {
				Navigation.dismissModal();
                if (response.e===0) {
                    store.save('token', {token: response.token, tokenSecret : response.tokenSecret})
					.then(() => {
						this.props.navigator.resetTo({
							screen: 'dar.main',
							title: 'Welcome',
						});
					})
					.catch(() => {
						Toast.show({
							text: "Error al guardar Tokens",
							position: 'bottom',
							buttonText: 'Okay'
						})
					});
                }else {
                    Toast.show({
                        text: response.mes,
                        position: 'bottom',
                        buttonText: 'Okay'
                    });
                }
            }) 
            .catch((err) => {
				Navigation.dismissModal();
                Toast.show({
					text: "Error de Conexión",
					position: 'bottom',
					buttonText: 'Okay'
				});
            })
        }else {
            Toast.show({
                text: 'Debes ingresar las Credenciales',
                position: 'bottom',
                buttonText: 'Okay'
            })
        }
    }

    handlerOptions() {
        this.props.navigator.resetTo({
            screen: 'dar.register',
            title: undefined,
        });
    }

    render() {

        return (
			<Root >
				<ScrollView style={styles.scrollView}>
					<View style={styles.container}>
						<Image
							resizeMode={'center'}
							style={{width: 280, height:280}}
							source={require("./img/logorojo.png")}
						/>
						<View style={styles.content}>
							<TextInput
								style={styles.inputText}
								onChangeText={(text) => this.dataSource.mail = text}
								placeholder="Correo Electronico"
								keyboardType={'email-address'}
							/>
							<TextInput
								style={styles.inputText}
								onChangeText={(text) => this.dataSource.password = text}
								placeholder="Contraseña"
								secureTextEntry={true}
							/>
							<TouchableNativeFeedback onPress={() => this.handlerSubmit()}>
								<View style={styles.btn}>
									<Text style={styles.btn_text}>Entrar</Text>
								</View>
							</TouchableNativeFeedback>
							<TouchableNativeFeedback  onPress={() => this.handlerOptions()}>
								<View style={styles.btn}>
									<Text style={styles.btn_text}>Registrate</Text>
								</View>
							</TouchableNativeFeedback>
						</View>
					</View>
				</ScrollView>
			</Root >
        );
    }
}

const styles = StyleSheet.create({
	scrollView: {
        flex: 1,
        backgroundColor: '#17afbd',
	},
    container: {
        flex: 1,
        backgroundColor: '#17afbd',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        width: Dimensions.get('window').width * 0.8,
        height: 'auto',
    },
    title: {
        fontSize: 20,
        color: 'white',
    },
    inputText: {
        backgroundColor: '#99e5f3',
        color: 'white',
        marginBottom: 4,
        borderBottomColor: 'transparent',
        paddingLeft: 5,
        paddingRight: 5,
        width: Dimensions.get('window').width * 0.8
    },
    btn: {
        position: 'relative',
        height: 35,
        margin: 8,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
    },
    btn_text: {
        color: '#e52427',
    }
})
   
export default Login;