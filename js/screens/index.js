import {Navigation} from 'react-native-navigation';

import Splash from './views/Splash';
import Login           from './views/Login';
import AccountRegister from './views/Register'; 
import Main            from './views/Main';
import Drawer from './views/Drawer';

import Loader from './views/Loader';

import Camera from './views/Camera';
import VideoPlayer from './views/Video';
import ImageView from './views/Image';


import Account         from './views/Account';
import Challenge       from './views/Challenge';
import Donations       from './views/Donations';
import Foundations     from './views/Foundation';
import Instructions    from './views/Instructions';
import Multimedia      from './views/Multimedia';
import Points   	   from './views/Points';
import PrivacyNotice   from './views/PrivacyNotice';
import Winner          from './views/Winners'; 

export default function () {
	Navigation.registerComponent('mx.somosdar.Drawer', () => Drawer);
	Navigation.registerComponent('mx.somosdar.Loader', () => Loader);
	Navigation.registerComponent('mx.somosdar.Splash', () => Splash);
  
  
  
	Navigation.registerComponent('dar.register' , () => AccountRegister)
	Navigation.registerComponent('dar.login'    , () => Login);
	Navigation.registerComponent('dar.main'     , () => Main);
  
  
	Navigation.registerComponent('mx.somosdar.Camera', () => Camera);
	Navigation.registerComponent('mx.somosdar.VideoPlayer', () => VideoPlayer);
	Navigation.registerComponent('mx.somosdar.ImageView', () => ImageView);
  
	Navigation.registerComponent('dar.account'  , 		() => Account);
  	Navigation.registerComponent('dar.challenge', 		() => Challenge);
  	Navigation.registerComponent('dar.donations', 		() => Donations);
  	Navigation.registerComponent('dar.foundations', 	() => Foundations);
  	Navigation.registerComponent('dar.instructions',    () => Instructions);
  	Navigation.registerComponent('dar.multimedia', 		() => Multimedia);
  	Navigation.registerComponent('dar.points',  		() => Points);
  	Navigation.registerComponent('dar.privacynotice',   () => PrivacyNotice);
  	Navigation.registerComponent('dar.winner',          () => Winner);
  
  
  
  
}
